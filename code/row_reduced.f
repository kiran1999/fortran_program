        program row_reduced
        end program row_reduced


        subroutine read_matrix(M, nr, nc)
            
c           Subroutine to read a matrix from the user.
c           Subroutine takes 3 arguments 
c           M := Actual Matrix to read
c           nr: Number of rows
c           nc: Number of columns

            implicit none
            real, dimension(10, 10) :: M
            integer:: nr, nc, i, j

            print *, "Enter the elements of the matrix"
            do i=1, nr
                read *, (M(i, j), j=1, nc)
            end do
        end subroutine read_matrix

        subroutine reduce_current_row(M, r, c, n)
c           Subroutine to calculate the row reduced form of
c           A given matrix.
c           M : The actual matrix to reduce
c           r:  Number of rows
c           c:  Number of columns
c           n:  The current row

            implicit integer (i, j, k)
            real, dimension(10, 10):: M
            integer:: r, c, n

        end subroutine reduce_current_row