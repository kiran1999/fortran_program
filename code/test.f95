module practice 
    implicit none

    type interval
        real:: lower, upper
    end type

contains
    function add_interval(a, b)
        type(interval)      :: add_interval
        type(interval), intent(in):: a, b

        add_interval%lower = a%lower + b%lower
        add_interval%upper = a%upper + b%upper
    end function add_interval

    ! interface operator(+)
    !     module procedure add_interval
    ! end interface
    
end module practice

program test
    use practice
    implicit none

    interface operator(+)
        module procedure add_interval
    end interface

    type(interval):: a, b, c
    a%lower = 1
    a%upper = 2

    b%lower = 3
    b%upper = 9

    c = a + b
    print *, c%lower, c%upper 


end program test
