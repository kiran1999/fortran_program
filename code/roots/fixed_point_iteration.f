        program main

            implicit none
            external phi, phi_prime

            x_start = 0
            epsilon = 1e-5
            
            call fixed_point_iteration(phi, phi_prime, x_start, epsilon, 
     * root, iters)
            
            if (iters < 0) then
                stop
            end if

            error = abs(f(root)-root)
            print*, x_start, root, error, iters, epsilon

        end program main

        subroutine fixed_point_iteration(f, f_prime, x_start, epsilon, root, iters)

            ! The subroutine to run the fixed point iteraton method.
            ! The inputs are given by

            ! # Inputs
            ! f:
            !   The Fucntion to run fixed point iteration

            ! f_prime:
            !   The derivative function of f

            ! x_start:
            !   The starting value of the iteration

            ! epsilon:
            !   Error tolarence.

            ! # Outputs
            ! root:
            !   To write the root of the equation.
            ! iters:
            !   Number of iterations. If it is negative, it implies
            ! maximum iteration count has exceeded.

            implicit double precision (a-h, o-z)
            integer, PARAMETER  :: MAXITER = 10000

            intent(in)          :: x_start, epsilon
            intent(out)         :: root, iters

            x = x_start         ! copy the value of x to a different variable.

            do iters = 1, MAXITER
                x_new = f(x)
                error = abs( x_new - x)

                if (error < epsilon) then
                    root = x_new
                    return
                else if ( abs(f_prime(x_new)) > 1) then
                    
                    stop "derivative is greater than 1, cannot continue"
                end if

                x = x_new
            end do
            if (iters == MAXITER+1) then
                print *, "Maximum number of iterations has exceeded"
                iters = -1
                root = x
            end if

        end subroutine fixed_point_iteration
