program main
    implicit none
    double precision, external :: f
    double precision    :: a, b, fa, fb, x, fx, eps
    integer             :: iter

    ! Maximum number of iteration to perform.
    integer, parameter          :: MAXITER = 10000

    ! The function will be linked later.
    ! The function `f` is in the function `function.f`.

    ! to compile:
    !   $ gfortran regula_falsi.f function.f -o regula_falsi
    ! to run:
    !   $ ./regula_falsi
    
    ! read the initial values (a, b) and the epsilon

    read *, a, b, eps

    ! a   :    The initial value a
    ! b   :    The initial value b
    ! eps :  The error tolarence.

    fa  = f(a)       ! the value of f at a
    fb  = f(b)       ! the value of f at b

    do iter = 1, MAXITER

        ! Run one scheme and find the value of new x
        x   = a - (b - a) / (fb - fa)* fa   ! the new x
        fx  = f(x)      ! The value of f at x

        if ( abs(fx) < eps ) then
            ! We found our solution. Print the solution
            print '(2A15)', "ROOT ", "ERROR "
            print *, "==================================="
            print 111, x, abs(fx)
111         format (2F15.10)

            ! and exit the loop
            exit

        else if ( fx*fa < 0 ) then
            ! The new interval is now (a, x)

            b       = x       ! set the new b
            fb      = fx      ! set the new value of fb
            ! and continue
        else if (fx*fb < 0) then
            ! the new interval is now (x, b)
            a       = x         ! set the new value of a
            fa      = fx        ! set the new value of fa
        else
            ! No root exits
            stop "No root exits in the interval"

        end if
    end do

    if ( iter > MAXITER ) then
        print *, "Maximum number of iterations exceeded"
    end if

end program main
