! This module contains all ther required functions
! for fixed point iteration and regula
! falsi method for calculating roots of the equation.


function f(x) result(r)
    implicit none

    double precision :: x, r

    ! The function to run fixed point iteration
    r = x**2 - 8*x + 15
    
end function f

function phi(x)
    implicit double precision (a-h, o-z)

    ! For the fixed point iteration

    phi = 15 / ( 8 - x)
end function phi

function phi_prime(x)
    ! derivative of phi
    implicit double precision (a-h, o-z)
    
    phi_prime = 15 / (8-x)**2
end function phi_prime
