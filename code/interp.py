from sympy import *
import numpy as np


def func(xs, ys):
    x = Symbol('x')
    f = 0
    n = len(xs)

    for i in range(n):
        f_temp = ys[i]
        for j in range(n):
            if i == j: continue

            f_temp *= (x-xs[j])/(xs[i]-xs[j])
        f += f_temp
    return f.simplify()


if __name__ == '__main__':
    xs = [1, 2, 3]
    ys = [5, 6, 7]

    f = func(xs, ys)
    init_printing(use_utf=True)
    print(f)
