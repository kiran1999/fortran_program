c       File:       Weddlesrule.f
c       Author:     Kiran Ajij
c       Desc:       Contains subroutine for integration using
c                   weddle's Rule

        subroutine weddle(f, a, b, nsteps, ans)
            ! Everything is double precisiion
            implicit double precision (a-h, o-z)
            dimension x(7)
            dimension coeff(7)

            intent(in):: a, b, nsteps
            ! intent(inout):: f
            intent(out):: ans

            coeff = (/ 1, 5, 1, 6, 1, 5, 1 /)

            ! N must be a multiple of 6, if it isn't, then
            ! make is a multiple of six

            n = nsteps
            if ( mod(n, 6) /= 0 ) n = (n/6 + 1) * 6

            M = n / 6       ! Number of iterations to be performed
            h = (b-a) / n   ! Step size

            s = 0           ! To store the cumulative sum
            do i = 0, M-1
                j = i*6
                x = (/ (a+(j+k)*h, k=0, 6) /)
                ! print *, (x(k), k=1, 6)
                do k=1, 7
                    s = s + coeff(k)*f(x(k))
                end do
            end do

            ans = (3.0/10.0)*h*s
        end subroutine weddle
