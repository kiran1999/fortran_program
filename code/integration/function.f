c       Contains the function to be integrated.
c       The method for integratin and the actual contains in another file
        function f(x) result(y)

            ! Everything is double precision. Can be done without using
            ! implicit typing, by using '-freal-4-real-8' in the gfortran
            ! compilation options.

            ! Given an input x the function returns the f(x).

            implicit double precision (a-h, o-z)

            y = x**2

        end function f

        double precision function trig(x)
            implicit double precision (a-h, o-z)
            trig = sin(x)
        end function trig 

        double precision function power5(x)
            double precision:: x
            power5 = x ** 5
        end function power5
            
            