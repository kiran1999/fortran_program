c       The actual code that puts everything together

        program integraion
            implicit double precision (a-h, o-z)
            external f, zeta, trig, power5

            character (len=40):: brk
            brk = "==========================================="
            print *, "Enter the number of test cases"
            read *, nTestCase

            print '(A40, /)', "THE INTEGRATION TABLE IS GIVEN BELOW"
            print 222, "A  ", "B  ", "N  ", "SIMPSONS  ", "WEDDLE'S"
222         format (2x, 2A15, A12, 2A15)
            print '(2x, 2A, /)', brk, brk
            do i=1, nTestCase
                ! print *, "Enter the upper limits of integraion"
                read *, a, b
                read *, n

                if (a >= b ) then
                    print *, "Limits must be a > b"
                    cycle
                end if

                ! print *, "Enter the number of subdivisions"

                if ( n < 1) then
                    print *, "N must be >= 1"
                    cycle
                end if

                call simpsons(power5, a, b, n, simp)
                call trapezoidal(poer5, a, b, n, trap)
                call weddle(power5, a, b, n, wedd)

                print 111, a, b, n, simp, wedd
111             format (2x, 2F15.7, I12, 2F15.7)
            end do
        end program integraion
