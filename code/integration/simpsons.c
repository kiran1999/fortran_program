/*
    File        :       Problems.c
    Author      :       Kiran Ajij
    Description :       Implelementing the simpsons
                        Method for numerical integration

    Build       :       `gcc problems.c -lm -o simpsons`
    Run         :       `./simpsons < input.txt > output.txt`
*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

double f(double x){
    /* The function to be Integrated */
    return x*x;      /* Si(x) */
}

double simpsons( double (*f)(double), double a, double b, int n ){
    /*
    Function to calculate the simpsons 1/3 integral on the given function
    Parameters:
        :f: The function to be integrated. Must be of the form double(double).
        :a: Lower limit of the integration.
        :b: Upper limit of the integration.
        :n: Number of sub divisions to be used.
            If provided N is odd, then it is rounded to a even number.
    */

    int coeffs[] = {1, 4, 1};      /* The coefficients for simpsons */
    int m;                  /* First loop counter */
    double h;               /* step size */
    double sum;             /* To store the accumulated sum */
    double integral;        /* The value of the integration */
    int i, j, k;            /* Loop counters */

    /* Round n to the closest even number (+1) */
    if (n % 2 != 0) n++;

    /* Calculate the values of h and m to be used by the program*/
    h = (b-a) / n;
    m = n/2;

    /* Initialize the sum */
    sum = 0;

    /* Begin the simpson's sum formula */
    for (i=0; i<m; i++){
        /* First loop works on one section of the Input interval
         * i.e. for intervals {x0, x1, x2, x3, ...}, this loop
         * works on {(x0, x1, x2), (x3, x4, x5)} format.
         */
        
        j = 2*i;
        for (k=0; k<3; k++){
            /* This loop works calculating the simpsns sum on
             * the slice {xi, x_i+1, x_i+2} 
             */
            sum += f( a+(j+k)*h ) * coeffs[k];
        }
    }

    /* Now calculate the integral given the sum and return it */
    integral = (h/3.0) * sum;
    return integral;
}

int main(){

    double a, b;        /* to store the lower and upper limits */
    int n, k;           /* To store the test case and subdivs */
    int i;              /* Loop counter */
    double integral;    /* to store the integral */
    double f(double);   /* The function to be integrated */

    // printf("Enter number of test cases\t");
    scanf("%d", &k);

    printf("%10s %10s %10s %15s\n", "a  ", "b  ", "n  ", "Integral ");
    printf("  ==============================================\n");
    for ( i=0; i<k; i++){
        // printf("Please Enter the lower and upper limit of Integration\t");
        scanf("%lf %lf", &a, &b);
        // printf("Enter number of subdivisions\t");
        scanf("%d", &n);

        integral = simpsons(sin, a, b, n);
        printf("%10.5lf %10.5lf %10d %15.10lf\n", a, b, n, integral);

    } 

}