# Simpsons 1/3 Rule For Numerical Integration.

In numberical analysis, Simpson's 1/3 rule is derived from the
Newton-Cotes' Rule for integration by setting `n=3`.

## Simpson's 1/3 rule

---

for the interval {a, a+h, a+2h}, The rule for integration of
the function `f` is given by
Sum = (h/3)(f(a) + 4f(a+h) + f(b)).
Thus this method has degree of precision `4`.

The composite formula for integration is given by the rule below

## Composite Formula

---
