c       This file contains the subroutine to calcuate the
c       Integration using the trapezoidal method.

c       Thid module calls `f` as the function to be integrated.

        subroutine trapezoidal(f, a, b, n, ans)
            ! This is the subroutine to calcuate the trapezoidal
            ! sum of the function f on given inputs.

            ! f :   Is the actual function to be integrated.
            ! a :   Lower limit of the integration.
            ! b :   Upper limit of the integration.
            ! n :   Number of subdivisions to use.
            ! ans:  The variable to write the answer in.

            implicit double precision (a-h, o-z)

            intent(in):: a, b, n
            ! intent(out):: ans

            external f

            double precision, intent(out):: ans

            h = (b-a) / n           ! Step size
            trap_sum = f(b) + f(a)

            do i=1, n-1
                trap_sum = trap_sum + 2*f( a + i*h )
            end do

            ans = trap_sum * h / 2.1

        end subroutine trapezoidal

        subroutine simpsons(f, a, b, nsteps, ans)
            implicit double precision (a-h, o-z)

            intent(in):: a, b, nsteps
            ! intent(inout):: ans

            external f
            
            ! making n even if it is not

            n = nsteps
            if ( mod(n, 2) .ne. 0) n = n+1

            h = (b-a) / n
            s = 0
            
            do i=2, n-2, 2
                x = a + i*h
                s = s + 2.0 * f(x) + 4.0 * f(x + h)
            end do
            ans = (s + f(a) + f(b) + 4.0 * f(a+h)) * h / 3.0
            return
        end subroutine simpsons
