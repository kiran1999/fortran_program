program name
    implicit none
    real, dimension(10):: x
    real:: sum, mean, stddev
    integer:: i

    do i=1, 10
        read *, x(i)
    end do

    sum = 0
    mean = 0

    do i=1, 10
        sum = sum + x(i)
    end do

    mean = sum / 10

    sum = 0
    do i=1, 10
        sum = (x(i) - mean) ** 2
    end do

    stddev = sqrt(sum / 10)

    print *, "mean and std deviation is given by"
    print *, mean, stddev

end program name