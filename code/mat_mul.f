c       Program to calculate the matrix multiplication
c       Author:         Kiran

        Program matmul
            implicit none
            real, dimension(10, 10) :: A, B, C
            integer:: ar, ac, br, bc, cr, cc

            print *, "Enter the size of the first matrix"
            read *, ar, ac

            call read_matrix(A, ar, ac)

            print *, "Matrix A"
            print *, "==========="
            call print_matrix(A, ar, ac)
            print *,

            print *, "Enter the size of the second matrix"
            read *, br, bc

            call read_matrix(B, br, bc)
            print *, "Matrix B"
            print *, "==========="
            call print_matrix(B, br, bc)
            print *,

            if (ac .ne. br) then
                stop "Invalid Size of Matrices"
            end if

            call mat_mul(A, ar, ac, B, br, bc, C, cr, cc)
            print *, "Matrix C"
            print *, "==========="
            call print_matrix(C, cr, cc)

        end program matmul

        subroutine read_matrix(M, nr, nc)
            implicit none

            real, dimension(10, 10) :: M
            integer                 :: nr, nc, i, j

            print *, "Enter the elements of the matrix"
            do i=1, nr
                read *, (M(i, j), j=1, nc)
            end do

        end subroutine read_matrix

        subroutine print_matrix(M, nr, nc)
            implicit none
            real, dimension(10, 10) :: M
            integer:: nr, nc, i, j

            do i=1, nr
                print "(10F15.5)", M(i, 1:nc)
            end do
        end subroutine print_matrix

        subroutine mat_mul(A, ar, ac, B, br, bc, C, cr, cc)
            implicit integer (i, j, k)

            real, dimension(10, 10) :: A, B, C
            integer:: ar, ac, br, bc, cr, cc

            cr = ar
            cc = bc

            do i=1, cr
                do j=1, cc
                    C(i, j) = 0
                end do
            end do

            do i=1, ar
                do j=1, bc
                    do k=1, ac
                        c(i, j) = c(i, j) + a(i, k) * b(k, j)
                    end do
                end do
            end do

        end subroutine mat_mul
