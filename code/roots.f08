program roots

    ! This is a simple program to determine 'real' roots of a 2nd degree polynomial.
    ! 
    ! :inputs:  The program takes a,b,c as inputs, where the equation is of the form
    !                       a x^2 + b x + c = 0     given a not equals 0
    !
    ! :outputs: Prints out the real roots of the equation. If the discriminant, i.e.
    !           D = b^2-4ac < 0, it prints out complex roots, otherwise it prints out4
    !            the two roots x1 and x2.

    implicit none

    ! a, b, c are to be read. we store the discriminant in 'd' and the sqrt of 'd' in 'dd'
    real:: a, b, c, d, dd

    ! to store the roots x1, x2 of the equation 
    real:: x1, x2

    ! take the user input by printing a message
    print *, "Please enter a, b, c"
    read *, a, b, c

    ! calculate the discriminant
    d = b**2 - 4*a*c

    ! check if d<0
    if (d < 0) then
        ! if d<0, we have complex roots, thus we print out the message and exit
        print *, "Complex roots"
        stop
    end if

    dd = sqrt(d)                ! otherwise store the sqrt(d) in dd

    ! and finally calcuate the roots of the equation
    x1 = (-b + dd) / 2/ a
    x2 = (-b - dd) / 2/ a    

    ! print out the results
    print *, "The roots of the equation are"
    print *, x1, x2
    
end program roots