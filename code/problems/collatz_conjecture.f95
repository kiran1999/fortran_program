!   Collatz sequence is given by
!   n' = n/2 if n is even
!      = 3n+1 if n is odd

! given any number as the starting number, the sequence always
! repeate become ...->4->2->1->4->2->1->...

program main
    implicit none
    integer(kind=8)         :: n
    do
        print *, "Enter the value of n"
        read *, n
        call Collatz(n)
    end do
end program main

subroutine collatz(n)
    implicit none
    integer(kind=8)  :: n

    print *, n
    do while (n /= 1)
        if (n < 0) then
            print *, "Negative number in the sequence"
        else if (mod(n, 2) == 0 ) then
            n = n / 2
        else
            n = 3*n+1
        end if
        print *, n
    end do
end subroutine collatz