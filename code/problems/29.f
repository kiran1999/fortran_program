c       Compute emelent wise sum/product/..
        program matrix_manipulation
            real, dimension(5, 5):: M1, M2, S, P, D, SS
            
            print *, "Enter Matrix 1"
            do i=1, 5
                read *, (M1(i, j), j=1, 5)
            end do

            print *, "Enter Matrix 2"
            do i=1, 5
                read *, (M2(i, j), j=1, 5)
            end do

            print "(/, A)", "Matrix 1"
            call print_matrix(M1)

            print "(/, A)", "Matrix 2"
            call print_matrix(M2)

            ! sum
            do i=1, 5
                do j=1, 5
                    S(i, j) = M1(i, j) + M2(i, j)
                end do
            end do

            ! product
            do i=1, 5
                do j=1, 5
                    P(i, j) = M1(i, j) * M2(i, j)
                end do
            end do

            ! division
            do i=1, 5
                do j=1, 5
                    D(i, j) = M1(i, j) / M2(i, j)
                end do
            end do
            
            ! substruction
            do i=1, 5
                do j=1, 5
                    SS(i, j) = M1(i, j) - M2(i, j)
                end do
            end do

            print "(/, A)", "Matrix S"
            call print_matrix(S)
            print "(/, A)", "Matrix P"
            call print_matrix(P)
            print "(/, A)", "Matrix D"
            call print_matrix(D)
            print "(/, A)", "Matrix SS"
            call print_matrix(SS)

            ! way #2 (without using loops)
            S = M1 + M2
            P = M1 * M2
            D = M1 / M2
            SS = M1 - M2

            print "(/, A)", "Matrix S"
            call print_matrix(S)
            print "(/, A)", "Matrix P"
            call print_matrix(P)
            print "(/, A)", "Matrix D"
            call print_matrix(D)
            print "(/, A)", "Matrix SS"
            call print_matrix(SS)

        end program matrix_manipulation

        subroutine print_matrix(M)
            real, dimension(5, 5):: M

            print "(2x, 1A)", "==============================="
            do i=1, 5
                print "(5F15.7)", (M(i, j), j=1, 5)
            end do
        end subroutine print_matrix