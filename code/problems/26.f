c       Read 10 values from the user and store them in an array
c       Then print the square of each numbers

        program read_array
            real, dimension(10):: x

            read *, (x(i), i=1, 10)

c           way #1
            print "(2F10.5)", (x(i), x(i) ** 2, i=1, 10)

c           Way #2
            do i=1, 10
                print *, x(i), x(i)**2
            end do
        end program read_array
