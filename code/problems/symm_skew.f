c       Program to detect if a given matrix is symmetric or
c       Skew-Symmetric matrix.

        program symm_skew
            implicit none
        
            integer                  :: nr, nc
            INTEGER                  :: i, j
            LOGICAL:: is_symmetric, is_skew_symmetric
            real, dimension(10, 10)  :: mat

            print *, "Enter dimenstion of matrix"
            read *, nr, nc

            print *, "Enter Elements of the matrix"

            do i=1, nr
                read *, (mat(i, j), j=1, nc)
            end do

            print *, "Symmetric", is_symmetric(mat, nr, nc)
            print *, "Skew Symmetric", is_skew_symmetric(mat, nr, nc)

        end program symm_skew

        function is_symmetric(mat, nr, nc)
            implicit none

            integer, intent(in)     :: nr, nc
            real, dimension(10, 10) :: mat
            logical                 :: is_symmetric
            
            ! Our loop counters
            INTEGER                 :: i, j

            do i=1, nr
                do j=1, nc
                    if ( mat(i, j) /= mat(j, i) ) then
                        is_symmetric = .false.
                        return
                    end if
                end do
            end do

            is_symmetric = .true.
        end function is_symmetric

        function is_skew_symmetric(mat, nr, nc)
            implicit none

            integer, INTENT(IN)         :: nr, nc
            real, dimension(10, 10)     :: mat
            logical                     :: is_skew_symmetric

            ! Loop Counter
            INTEGER                     :: i, j

            do i=1, nr
                do j=1, nc
                    if ( mat(i, j) /= -mat(j, i)) then
                        is_skew_symmetric = .false.
                        return
                    end if
                end do
            end do

            is_skew_symmetric = .true.
        end function is_skew_symmetric
