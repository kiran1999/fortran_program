!       A Program of row reduced form of a matrix
!       Author  : Kiran Ajij

        program main

            real, dimension(10, 10) :: mat
            integer:: pr, pc, elem_type

            read *, nr, nc, elem_type
            do i=1, nr
                read *, (mat(i, j), j=1, nc)
            end do

            pr = 0; pc = 0

            call do_pivot(mat, pr, pc, nr, nc)
            call row_reduce(mat, pr, pc, nr, nc, elem_type)

            call print_matrix(mat, nr, nc)
                
        end program main

        recursive subroutine row_reduce(mat, pr, pc, nr, nc, elem_type)
            ! The subrotuine that handles the reduction of a row.
            ! Inputs:
            !   mat:    The matrix to be reduced in the form
            !   pr :    Pivot row
            !   pc :    Pivot column
            !   nr  :   Number of rows
            !   nc  :   Number of columns
            !   elem_type:  Type of elemination to use
            !               1 - gaussian
            !               2 - echelon
        
            integer         :: pr, pc
            real, dimension(10, 10) :: mat
            integer, intent(in)  :: nr, nc, elem_type

            if (elem_type /= 1 .and. elem_type /=2 ) return
            
            ! Note that pivot positions must be non zero
            
            if (elem_type == 1) call gaussian_elem(mat, pr, pc, nr, nc)
            if (elem_type == 2) call reduce_echelon(mat, pr, pc, nr, nc)

            ! call print_matrix(mat, nr, nc)
            
            ! After callng gaussian_elem on the matrix, now find the next pivots
            call do_pivot(mat, pr, pc, nr, nc)
            if (pr == -1) return
            call row_reduce(mat, pr, pc, nr, nc, elem_type)
        
        end subroutine row_reduce

        subroutine gaussian_elem(mat, pr, pc, nr, nc)

            integer         :: pr, pc
            real, dimension(10, 10) :: mat
            intent(in)  :: nr, nc
            
            ! Divide by the scale
            scale = mat(pr, pc)
            mat(pr, 1:nc) = mat(pr, 1:nc) / scale

            do i=pr+1, nr
                mat(i, 1:nc) = mat(i, 1:nc) - mat(pr, 1:nc)*mat(i, pc) 
            end do

        end subroutine gaussian_elem

    
        subroutine reduce_echelon(mat, pr, pc, nr, nc)

            integer         :: pr, pc
            real, dimension(10, 10) :: mat
            intent(in)  :: nr, nc
            
            ! Divide by the scale
            scale = mat(pr, pc)
            mat(pr, 1:nc) = mat(pr, 1:nc) / scale

            do i= 1, nr
                if (i == pr) cycle
                mat(i, 1:nc) = mat(i, 1:nc) - mat(pr, 1:nc)*mat(i, pc) 
            end do

        end subroutine reduce_echelon

        subroutine do_pivot(mat, pr, pc, nr, nc)
            
            real, dimension(10, 10) :: mat
            real, DIMENSION(10)  :: temp
            integer, intent(in)     :: nr, nc
            integer                 :: pr, pc

            ! If the next usual pivot is non-zero, set it as the new pivot
            if ( mat(pr+1, pc+1) /= 0) then
                pr = pr+1
                pc = pc+1
                return
            end if

            ! Otherwise, IF it is zero, find the next pivot
            nnr = -1; nnc = -1
            do i=pc+1, nc
                do j=pr+1, nr
                    if ( mat(i, j) /= 0 ) then
                        nnr = j
                        nnc = i
                    end if
                end do
            end do

            if ( nnr /= -1 ) then
                ! There is a non zero pivot. so set it as the new pivot
                temp = mat(pr+1, :)
                mat(pr+1, :) = mat(nnr, :)
                mat(nnr, :) = temp

                pr = nnr
                pc = nnc
                return
            else
                pr = -1
                pc = -1
            end if
        end subroutine do_pivot

        subroutine print_matrix(mat, nr, nc)

            real, dimension(10, 10)     :: mat
            INTEGER, INTENT(IN)         :: nr, nc

            print '(/x2 A12)', "Printing Matrix"
            do i=1, nr
                print *, (mat(i, j), j=1, nc)
            end do
        end subroutine print_matrix
