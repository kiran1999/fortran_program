c       create an array of size 30 with the rule
c               x(1) = 1
c               x(2) = 2
c       x(i) = x(i-1) + x(i-2), i=3, 4, ..., 30

        program fib
            integer, dimension(30):: x
            integer:: i

            x(1) = 1
            x(2) = 1

            do i=3, 30
                x(i) = x(i-1) + x(i-2)
            end do
            print "(1I7)", x(3:)

        end program fib