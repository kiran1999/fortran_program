c       Program     :       lagrange_interpolation.f
c       Author      :       Kiran ajij
c       Description :       Calculate the interpolation polynomial
c                           Using Lagrange's Interpolation Method.

        program interp
            ! Maximum possible size of the sample is 10, And we define
            ! the arrays according to our needs.

            dimension x(10), y(10)

            ! First read the value of N which is the sample size of our
            ! data
            read *, n

            ! Now read the values of x's and y's from the user
            read *, (x(i), y(i), i=1, n)

            ! Print the values of the data in the screen for confirmation
            ! With some style

            print '(/14x, A))', "INPUT DATA"
            print '(2A15)', "X INPUT", "Y INPUT"
            print '(2x, A))', "=============================="
            print "(2F15.7)", (x(i), y(i), i=1, n)
            print '(2x, A, /)', "=============================="

            ! Now input the size of the required test points
            read *, nTestPoints

            ! For each test point, read the value of x and calcuate y(x)
            ! for the given x
            print '(2A15)', "X TEST", "Y TEST"
            print '(2x, A)', "=============================="
            do i=1, nTestPoints
                ! Read the test point
                read *, xin

                ! call the lagrang interpolation subroutine for `xin`
                call lagrange(x, y, n, xin, yout)
                print '(2F15.7)', xin, yout
            end do
        end program interp

        subroutine lagrange(x, y, n, xin, xout)
            ! The subroutine to calcuate the interpolation for a given
            ! x, in this case `xin`

            ! The interpolating formula is based on some polynomials f_i(x)
            ! such that 
            !       f_i(xj) = 1 if i = j
            !               = 0 if i != 0

            ! f_i(x) is calculated as
            !   f_i(x) = (x - x0)(x-x1)..(x-xn) / (xk-x0)(xk-x1)...(xk-xn)
            ! and finally adding the values of these polynomials evaluated
            ! at x=xin.
            
            dimension x(10), y(10)

            xout = 0.0          ! To store the accumulated sums of the polys
            do i=1, n
                ! First loop caluates each polynimals in the set
                ! {f_1, f_2, ... , f_n}
                ! The vales of the polynomials are calcuated using a loop and
                ! each loop adds another factor of the polynomial.

                prod = y(i)      ! To store the value of each round of poly

                do j=1, n

                    ! If j = i, then do not calcuate the factor, just skip
                    if (i .eq. j) cycle

                    ! Otherwise the product is given by
                    prod = prod * (xin - x(j)) / (x(i) - x(j))
                end do

                ! finally add the value of f_i(xin) to the variable `xout`

                xout = xout + prod

            end do
        end subroutine lagrange