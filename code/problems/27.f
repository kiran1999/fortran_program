c       Reverse the array

        program reverse_array
            integer:: x(10)

            read *, (x(i), i=1, 10)
            
            do i=1, 4
                tmp = x(i)
                x(i) = x(10-i+1)
                x(10-i+1) = tmp
            end do

            print "(2I5)", (i, x(i), i=1, 10)
        end program reverse_array