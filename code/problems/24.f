c      create an array of size 10 with the given rule
c               x(i) = i^2, i=1, 2, ..., 10

        program array_compreshension
            integer, dimension(10):: x(10)

c           way #1
            do i=1, 10
                x(i) = i**2
            end do
            print "(1I7)", x

c           way #2
            x = (/ (i**2, i=1, 10) /)
            print "(1I7)", x
        end program array_compreshension