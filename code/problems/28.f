c       Sort a given array of size 10
        program bubble_sort
            real, dimension(10):: x

            read *, (x(i), i=1, 10)

            do i=1, 9
                do j=1, 10-i
                    if (x(j) .gt. x(j+1)) then

c                       If the number is greater than the next number then
c                       just simply swap them around

                        tmp = x(j)
                        x(j) = x(j+1)
                        x(j+1) = tmp
                    end if
                end do
            end do

            print "(I7, F10.5)", (i, x(i), i=1,10)
        end program bubble_sort