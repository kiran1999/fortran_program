program sumOfIntegers

    ! This is a simple program to calculate the sum of first 1000 integers.

    implicit none

    integer:: i             ! our good old friend, loop counter
    integer:: sum           ! to store the partial sums

    sum = 0                 ! initialize the value to 0

    ! add i to the sum per loop
    do i=1, 1000
        sum = sum+i
    end do

    ! and finally print the value of sum
    print *, "The sum of first 1000 natural number is"
    print *, sum
    
end program sumOfIntegers