c       program     :    factorials.f
c       Author      :    Kiran
c       Description :    program to calculate and store factorials

        program factorial
            implicit integer (kind=8) (a-z)
            integer (kind=4):: i

            dimension facts(20)

            facts(1) = 1

            do i=2, 20
                facts(i) = i * facts(i-1)
            end do

            print 111, (i, facts(i), i=1,20)
111         format( I4,  I25)
        end program factorial