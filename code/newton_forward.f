c       Program         :       newton_forward.f
c       Author          :       Kiran
c       Description     :       Interpolation using Newton's Method
c                               Netwon Forward interpolation
c
        program newton_forward
            dimension x(10), y(10), d(10, 10)
            print *, "Please Enter the value of N"
            read *, n
c           Scanned the number of inputs to work with
c           Scan the input data form stdin
            read *, (x(i), y(i), i=1,N)

c           Now print out the scanned valuesfrom flask import app to stdout
            print 111, (x(i), y(i), i=1,N)
111         format (/2x, 2F15.10)

            do i=1, N-1
                d(i, 1) = y(i+1) - y(i)
            end do

            do i = 2, N-1
                do j = 1, n-i
                    d(j, i) = d(j+1, i-1) - d(j, i-1)
                end do
            end do

c           Now print the difference table
            call print_block()
c           call print_block()
            do i = 1, N-1
                print *, x(i), y(i), (d(i, j), j=1, n-i)
            end do
            print *, x(n), y(n)

c           Now check for some given input values
c           Read the number of values to test on
            read *, np

            do i=1, np
c               For each input value, calcuate the u
                read *, xp
                u = (xp - x(1)) / (x(2) - x(1))
c               Store the accumulated sum in s
c               store the previous term in p_term and calculte the next term
c               based on the formula
c                   t_{n+1} = t_n * (u-n) / (j+1)

                s = y(1)
                t_n = u

                do j = 1, n-1
                    s = s + t_n * d(1, j)
                    t_n = t_n * (u-j) / (j+1)
                end do
                print 222, xp, s
222             format ("F(", f6.3, ") = ", f15.10)
            end do


        end program newton_forward

        subroutine print_block
            print *, "===================================="
            print *, "DIFFERENCE TABLE"
            print *, "===================================="
        end subroutine print_block
