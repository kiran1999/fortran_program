module matrix_ops
    implicit none
    
contains
    subroutine print_name()
        implicit none
        write (*, *) "MATRIX OPERATION MODULE"
        write (*, *) "Author: Kiran Ajij"
    end subroutine print_name
    
end module matrix_ops