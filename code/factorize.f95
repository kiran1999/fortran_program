program factorize
    ! A simple program to break a number into it's prime factorization.
    ! It takes an input from the stdin and prints out the prime factorization
    ! in the given format
    !
    !       n       power
    !
    ! for exaple, the number 12 = 2^2 x 3
    ! thus this program prints out
    !           2       2
    !           3       1
    implicit none
   
    integer:: i, count, n           ! variables we'll need

    read *, n                       ! take input n from the user

    i = 2   ! 1 is not considered in prime factorization, so we'll
            ! start from 2

    do while (i <= n)
        ! while, i is less or equals n (we need equals for prime numbers), set
        ! the count(multiplicity) of the number to 0.
        count = 0

        do while ((mod(n, i)==0))
            ! while i| n, keep adding one to the count and reduce n by a factor i
            count = count + 1
            n = n / i
        end do

        ! finally if the count of the multiplicity is greater than zero, then
        ! print out the number with the count it appeared
        if (count /= 0) then
            print *, i, count
        end if

        ! and finally increment i by 1
        i = i+1
    end do
end program factorize
