program main
    implicit integer(kind=8) (a-z)
    logical         :: r

5   print *, "Enter the number"
    read *, n

    call decompose(n, k, q)

    call miller_rabin(n, r)
    if (r .eqv. .true.) then
        print *, "Inconclusive"
    else
        print *, "Composite"
    end if

    goto 5


end program main


subroutine miller_rabin(n, r)

    implicit integer(kind=8) (a-z)

    logical, intent(out)    :: r
    intent(in)              :: n
    integer(kind=8)         :: a

    call decompose(n, k, q)
    call random_integer(1, n-1, a)

    if ( mod(a**q, n) == 1) then
        r = .true.
        return
    end if

    do i= 0, k-1
        if (mod( a** ((2**j)*q ), n) == n-1) then
            r = .true.
            return
        end if
    end do 

    r = .false.
    return

end subroutine miller_rabin

subroutine decompose(n, k, q)
    implicit integer(kind=8) (a-z)
    intent(in)          :: n
    intent(out)         :: k, q

    k = 0
    rem = mod(n, 2)
    n_copy = n

    do while(rem == 0)
        k = k + 1
        n_copy = (n_copy/2)
        rem = mod(n_copy, 2)

    end do
    q = n_copy
end subroutine decompose

subroutine random_integer(l, u, a)
    implicit none

    integer(kind=8), intent(in)     :: l, u
    integer(kind=8), intent(inout)  :: a
    real                            :: rand

    ! Get a random number in 0-1
    call random_number(rand)

    ! Rescale it to the desired range 
    a = floor( l + (u-l+1)*rand )

end subroutine random_integer
