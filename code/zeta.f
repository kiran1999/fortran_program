        function zeta(s) result(r)
            double precision:: s, r
            integer:: i

            r = 0
            do i=1, 1000
                r = r + 1.0 / (i**s)
            end do

        end function zeta
